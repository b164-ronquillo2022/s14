console.log ("hello world");

//JS staements usually end with semicolon(;). are not required but we will use it to help us train to locate where a statements ends.
//Statements, Syntax and Comments
//Note: convention: Breaking up long statements into multiple lines is common practice.


//variables-used to store a data
// Declare Variables:
//Syntax: let/const varibleName;
//let/const/var is a keyworld is a keyword that is used in declaring a variable.

let myVariable = "Hello Again!";
console.log(myVariable);

let myVariable1

/*Guides in writing variables:
1. we cvan use "let" keyword followed by the varible name of your choosing and use the assignment operator (=) to assign a value.
2. Variable names should start with a lowercase character, use camelcase for multiple worlds
3. descriptive and indicative of the data it contains*/

let firtName = "Jane" // good variable name
let lastName = "Doe"
let pokemon = 25000   //bad

//2. It is better to start with lowercase letter. Because there are several keywords in JS that start in capital letter.

//3. Do not add spaces to your variable nbames. Use camel Case for multiple words, or underscore

let product_Description = "sample";

//Declaring and initializing variables
//Initializing variables - the instance when a variable is given it's initial/ starting value

//Syntax: let/const Variable = value

let productName = "desktop computer";
console.log (productName)

let productPrice = 18999;
console.log(productPrice)

const interest = 3.539;

//Reassigning variable values
productName = "Laptop";
console.log (productName)

// let variable can be reassigned with another value;
//let variable cannot be re declared within its scope;

let friend = "Kate";
let anotherFriend = "John"; //


//const cannot be updated or re-declared
//values of canstant cannot be change and will simply return an error
const pi = 3.14;
// pi = 3.14;



//Reassigning variables vs initializing variables
//let - we can declare a variable initializing variables 

let supplier;
//Initaialization is done after the variable has been declared

supplier = "John Smith Tradings";
console.log(supplier)

supplier = "Zuitt Store"
console.log(supplier);


//cons variables are variables with constant data. Therefore we should not re-declare, re-assign or even declare a const variable without initialization.

//var vs. let/const

//var - is also used in declaring a variable. var is an ECMA script (ES1) feature ES1 (JS 1997).

//let/const was introduce as a new feature in ES6(2015)

//what makes let/const different from var?

//hoisting - ther are issues when we used var in declaration to the top.

a=5;
console.log(a);
var a;

//Scope essentially means where these variables are available to use

//let/const can be used as a local and global scope
// a block is a chunk of code bounded by{}. A block lives in curly braces.
//anythging within curly braces is block
// so a variable declared 
// global
let outerVariable = "hello";

//block / local scope = {}




{
	let innerVariable = "hello gain!";
	console.log(innerVariable);

}

console.log(outerVariable);

// console.log(innerVariable); //is not defined



let productCode = 'DC017';
const productBrand = 'Dell';
console.log (productCode, productBrand);

//

//const let ="hello"


//string , numbers ,boolean, null, Undefined, Object


//Data Types

//Strings
//Strings are a series of characters that creates a word , aphrase . a sentence or anything related to creating text.
// Strings in javascript can be written using a single ('') or double ("")
//quotation.

let country = 'Philippines'
let province = "Metro Manila"

console.log(typeof country);
console.log(typeof province);

//Concatenating strings

// multiple string values can be combined to create a single string using the "+"
//symbol

let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

 
//Template literals (ES6) is the updated version of concatenation
//backticks `dvdv`
//expression ${}
console.log(`I live in the ${province}, ${country}`);

//Escape character \n //newline

let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

//\''

let message = 'John\'s employees went home early';
console.log(message);

message = "John's employees went home early"
console.log(message);


console.log( `Metro Manila

	          "Hello"

	          'Hello'

	          Philippines ${province}`);

//Numbers
//Integers/Whole Numbers

let headCount = 23;
console.log(typeof headCount);

//Decimal Numbers/Fractions
let grade = 98.7;
console.log(typeof grade);

//Exponential Notation

let planetDistance = 2e10;
console.log (typeof planetDistance);

//Combining variable with number data type and string
console.log("John's grade last quarter is " + grade);
console.log(`John's grade last quarter is ` + grade);



//boolean
//boolean values are normally used to store values relating to state of certain things
//this will be useful in further 

let isMarried = false;
let inGoodConduct = true;

console.log(typeof isMarried);
console.log(typeof inGoodConduct);


//Objects

//arrays are a special kind of data that is used to store multiple values . it can store multiple values.
//it can store different data types  but is normally


let grades = [98.7, 92.1, 90.2, 94.6];
console.log(typeof grades);

//different data types
//storing diff data types inside an array is not recommended because it will not make sense
//to join the context of programming.

let details = [ "John", "Smith", 32, true];
console.log(details);
//inde
console.log(grades [1], grades[0]);

//reassigning array elements

// let anime = ['one piece', 'one punch man', 'attack on titan'];
// console.log(anime);
// anime [0] = 'kimetsu no yaiba';
// console.log(anime);

const anime = ['one piece', 'one punch man', 'attack on titan'];
 console.log(anime);
anime [0] = 'kimetsu no yaiba';
 console.log(anime);

 //the keyword const is a little misleading.It does not define a constant value. It defines a constant reference to a value.

//objects
//
// let/const objectName = {propertyA: value, propertyB: value}

//They're used to create complex data that contains pieces of information that are relevant

let objectGrades = {firstQuarter: 98.7,
                     secondQuarter: 92.1,
                     thirdQuarter: 90.2,
                     fourthQuarter: 94.6
                 };

let person = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
}


console.log(`${person.fullName}`);

/* mini Activity
Mini Activity

1. Create 2 variables named firstName and lastName.
                -Add your first name and last name as strings to its appropriate variables.
                -Log your firstName and lastName variables in the console at the same time.



2. Create a variable called sentence.
                -Combine the variables to form a single string which would legibly and -understandably create a sentence that would say that you are a student of Zuitt.
                -Log the sentence variable in the console.


3. Create a variable with a group of data.
                        -The group of data should contain names from your favorite food.

4. Create a variable which can contain multiple values of differing types and describes a single person.
                        -This data type should be able to contain multiple key value pairs:
                                firstName: <value>
                                lastName: <value>
                                isDeveloper: <value>
                                hasPortfolio: <value>
                                age: <value> */


 let firstName = "Roldan";
 let last2Name = "Ronquillo";

console.log(firstName + " " + last2Name);

let sentence = "a student of Zuitt.";

console.log("Hi, I'm" + " " + firstName + " " + last2Name + " " + sentence);

let favFood = ["lasagna", "sinigang", "paksiw"];

console.log(favFood);


let diffValues = {
	firstName: "Roldan",
	lastName:"Ronquillo",
	isDeveloper: true,
	hasPortfolio: true,
	age: 37
}

console.log(diffValues);


//Number 

let numb1 = 5;
let numb2 = 6;
let numb3 = 5.5;
let numb4 = .5;


let numString1 = "5";
let numString2 = "6";

console.log(numString1 + numString2)//"56" strings were concatenated.
console.log(numb1 + numb2) //11

//Type Coercion/forced 
//is the suthomatic or implicit convertion of values from one data type to another
console.log(numString1 + numb1)//"55" resulted in concatenation
//Adding/ Concatenating a string and a number will result into astring.


//Mathematical operations (+,-*,/,%)(modulo-remainder)

//Modulo%

console.log (numb1 % numb2);//(5/6) = remainder 5
console.log (numb2 % numb1);//(6/5) = remainder 1

console.log(numb2 - numb1);

let product = (numb1 * numb2);

console.log(product);

//null
//intentional absence of a value; It is intentionally express the absence 

let girlfriend = null;
console.log(girlfriend);

let myNumber = 0;
let myString = '';

console.log(myNumber);
console.log(myString);

//using null compared to a 0 value and empty string is much better for readability purposes.



//undefined
//Represents no value.

let sampleVariable;
console.log(sampleVariable); //undefined

//One clear difference between undefined and null is that for undefined, no value created
//variable


//Function
//in JS it is aline or line/s/ or blocks of code that tell our app to perform a certain task when called/invoked.

//function declaration: it defines a function with the function and specified parameters.
/*

Syntax:

function functionName{
	code block/statements. the block of code will be executed once the function has been run/called/invoked.
}

   function keyword => defined a js function
   function block/statement => the statement which comprise the body otf the function.This is where code be executed.
   */

     // function printName() {
     // 	console.log("My name is John");
     // };


//Function eXPRESSION

//A FUNCTION EXPRESSION CAN BE STORED IN A VARIABLE

let variableFunction = function(){
	console.log("Hello World");
};

let funcExpression = function func_name() {
	console.log("Hello!");
}


//function invocation
//the code inside a function is executed when the function is called/invoked.

//lets invoke or call a function

// printName();

function declaredFunction() {
	console.log("Hello World");
};

declaredFunction();

//How about the function


variableFunction();

//Function Scoping

//JS scope
//global and local scope

//
// var faveCharacter = "Nezuko-chan";//global

function myFunction() {
	let nickName = "Jane"; // function scope
	console.log(nickName);

	
}

myFunction();
// console.log(nickName);//not defined
// console.log();//not defined


//Variables defined inside a function are not (visible) from outside the function.
//var/let const are quite similar when declared inside a function
//variables declared globally (outside of the function) have a global scope or it can access anywhere.

//Parameters and Arguments
//"name" is called a parameter

let name = "Jane Smith"

//parameter => acts as a named variable/container that exist only inside of the function
 function printName(name) {
     	console.log(`My name is ${name}`);
     };


     printName(name);

     //Multiple parameters

     function displayFullName(fName, lName, age) {
     	console.log(`${fName} ${lName} is ${age}`);

     };

     displayFullName("Judy", "Medalla", "jane", 67);

     //Providing less arguments than the expected parameters will authomatically assign an undefine value to the parameter.
     //prviding more arguments will not affect our function.


     //return keyword
     //The return keyword statement allows the output of afunction to be passed to the block of code that invoked the function.


     function createFullName(firstName, middleName, lastName) {
     	return `${firstName} ${middleName} ${lastName}`
     	console.log ("I will no longer run because the function's value/result has been returned");


     }

     let fullName1 =  createFullName("Tom", "Cruise", "Mapother")
     console.log(fullName1)

     //di siya gagana pag walang variable keyword

let fullName2 = displayFullName("William", "Bradley", "pitt");
console.log(fullName2);

let fullName3 = createFullName("jeffrey", "John", "Smith");
console.log (fullName3);

// Mini Activity
//         Create a function can receive two numbers as arguments.
//                 -display the quotient of the two numbers in the console.
//                 -return the value of the division

//         Create a variable called quotient.
//                 -This variable should be able to receive the result of division function

//         Log the quotient variable's value in the console with the following message:

//         "The result of the division is: <value of Quotient>"
//                 -use concatenation/template literals

//         Take a screenshot of your console and send it in the hangouts.


let input1 = (prompt("Enter first number"));
let input2 = (prompt("Enter second number"))

function divide(input1, input2) {
  return `${input1 / input2}`;             
}
let quotient = divide(input1,input2);  
alert(`${'The result of the division is:'+ " " + quotient}`);